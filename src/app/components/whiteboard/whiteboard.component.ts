import { Component, OnInit, OnDestroy } from '@angular/core';
import { TransmitterService } from '../../services/transmitter.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'app-whiteboard',
    templateUrl: './whiteboard.component.html',
    styleUrls: ['./whiteboard.component.css']
})
export class WhiteboardComponent implements OnInit {

    subscription: Subscription;

    constructor(private transmiter: TransmitterService) {

        this.subscription = transmiter.broadcastChannel$.subscribe(
            value => {
                console.log('WhiteboardComponent recieved: ' + JSON.stringify(value));
            }
        );
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    ngOnInit() {

    }

    raiseEvent() {
        
        this.transmiter.broadcast({ "WhiteboardComponent: ": 'event raised!' });
    }
}
