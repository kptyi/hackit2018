import { Component, OnInit, OnDestroy } from '@angular/core';
import { TransmitterService } from '../../services/transmitter.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'app-card-code',
    templateUrl: './card-code.component.html',
    styleUrls: ['./card-code.component.css']
})
export class CardCodeComponent implements OnInit {

    subscription: Subscription;

    constructor(private transmiter: TransmitterService) {

        this.subscription = transmiter.broadcastChannel$.subscribe(
            value => {
                console.log('CardCodeComponent recieved: ' + JSON.stringify(value));
            }
        );
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    ngOnInit() {

    }
}
