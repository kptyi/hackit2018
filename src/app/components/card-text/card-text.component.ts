import { Component, OnInit, OnDestroy } from '@angular/core';
import { TransmitterService } from '../../services/transmitter.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'app-card-text',
    templateUrl: './card-text.component.html',
    styleUrls: ['./card-text.component.css']
})
export class CardTextComponent implements OnInit {

    subscription: Subscription;

    constructor(private transmiter: TransmitterService) {

        this.subscription = transmiter.broadcastChannel$.subscribe(
            value => {
                console.log('CardTextComponent recieved: ' + JSON.stringify(value));
            }
        );
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    ngOnInit() {

    }

    raiseEvent() {
        this.transmiter.broadcast({ "CardTextComponent: ": 'event raised!' });
    }
}
