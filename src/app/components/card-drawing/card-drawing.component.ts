import { Component, OnInit, OnDestroy } from '@angular/core';
import { TransmitterService } from '../../services/transmitter.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'app-card-drawing',
    templateUrl: './card-drawing.component.html',
    styleUrls: ['./card-drawing.component.css']
})
export class CardDrawingComponent implements OnInit {

    subscription: Subscription;

    constructor(private transmiter: TransmitterService) {

        this.subscription = transmiter.broadcastChannel$.subscribe(
            value => {
                console.log('CardDrawingComponent recieved: ' + JSON.stringify(value));
            }
        );
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    ngOnInit() {

    }
}
