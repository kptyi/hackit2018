import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardDrawingComponent } from './card-drawing.component';

describe('CardDrawingComponent', () => {
  let component: CardDrawingComponent;
  let fixture: ComponentFixture<CardDrawingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardDrawingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardDrawingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
