import { Component, OnInit, OnDestroy } from '@angular/core';
import { TransmitterService } from '../../services/transmitter.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'app-agenda',
    templateUrl: './agenda.component.html',
    styleUrls: ['./agenda.component.css']
})
export class AgendaComponent implements OnInit, OnDestroy {

    subscription: Subscription;

    constructor(private transmiter: TransmitterService) {

        this.subscription = transmiter.broadcastChannel$.subscribe(
            value => {
                console.log('AgendaComponent recieved: ' + JSON.stringify(value));
            }
        );
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    ngOnInit() {

    }

    raiseEvent() {
        this.transmiter.broadcast({ "AgendaComponent: ": 'event raised!' });
    }
}
