import { Component, OnInit, OnDestroy } from '@angular/core';
import { TransmitterService } from './services/transmitter.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    providers: [TransmitterService]
})
export class AppComponent {
    title = 'app';
}
