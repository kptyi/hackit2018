import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class TransmitterService {

    private broadcastSource = new Subject<any>();

    broadcastChannel$ = this.broadcastSource.asObservable();

    broadcast(value: any) {
        this.broadcastSource.next(value);
    }
}
