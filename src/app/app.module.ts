import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { WhiteboardComponent } from './components/whiteboard/whiteboard.component';
import { CardTextComponent } from './components/card-text/card-text.component';
import { CardCodeComponent } from './components/card-code/card-code.component';
import { CardDrawingComponent } from './components/card-drawing/card-drawing.component';
import { AgendaComponent } from './components/agenda/agenda.component';
import { ChatBoxComponent } from './components/chat-box/chat-box.component';

import { TransmitterService } from './services/transmitter.service';


@NgModule({
    declarations: [
        AppComponent,
        WhiteboardComponent,
        CardTextComponent,
        CardCodeComponent,
        CardDrawingComponent,
        AgendaComponent,
        ChatBoxComponent
    ],
    imports: [
        BrowserModule
    ],
    providers: [TransmitterService],
    bootstrap: [AppComponent]
})
export class AppModule { }
